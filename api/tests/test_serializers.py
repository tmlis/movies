from django.test import TestCase
from ..serializers import MovieSerializer, CommentSerializer
from ..models import Movie, Comment


class MovieTest(TestCase):
    def setUp(self):
        self.movie_attributes = {
            'id': 0,
            'title': 'Green Book',
            'omdb_data': {'Title': {'Green Book'}}
        }

        self.movie = Movie.objects.create(**self.movie_attributes)
        self.serializer = MovieSerializer(instance=self.movie)

    def test_contains_fields(self):
        data = self.serializer.data
        self.assertEqual(set(data.keys()), {'id', 'title', 'omdb_data'})

    def test_title_field_content(self):
        data = self.serializer.data
        print(data['title'])
        self.assertEqual(data['title'], 'Green Book')


class CommentTest(TestCase):
    def setUp(self):
        self.comment_attributes = {
            'id': 0,
            'movie_id': 1,
            'text': 'some text'
        }

        self.comment = Comment.objects.create(**self.comment_attributes)
        self.serializer = CommentSerializer(instance=self.comment)

    def test_contains_fields(self):
        data = self.serializer.data
        self.assertEqual(set(data.keys()), {'id', 'movie_id', 'text'})

    def test_text_field_content(self):
        data = self.serializer.data
        self.assertEqual(data['text'], 'some text')
