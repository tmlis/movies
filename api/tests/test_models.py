from django.test import TestCase
from ..models import Movie, Comment


class MovieTest(TestCase):
    def setUp(self):
        self.movie = Movie.objects.create(title='Green Book', omdb_data={'Title': 'Green Book'})

    def test_create(self):
        self.assertEqual(self.movie.title, 'Green Book')
        self.assertEqual(self.movie.omdb_data, {'Title': 'Green Book'})


class CommentTest(TestCase):
    def setUp(self):
        self.comment = Comment.objects.create(movie_id=1, text='some text')

    def test_create(self):
        self.assertEqual(self.comment.movie_id, 1)
        self.assertEqual(self.comment.text, 'some text')
