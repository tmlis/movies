from django.test import TestCase
from ..services import omdb_movie
from rest_framework import status
from rest_framework.exceptions import APIException


class OmdbTest(TestCase):
    def setUp(self):
        self.valid_movie_title = 'Green Book'
        self.invalid_movie_title = 'invalid_movie_title'

    def test_get_valid_movie(self):
        response = omdb_movie(self.valid_movie_title)
        self.assertEqual(response['Title'], 'Green Book')

    def test_get_invalid_movie(self):
        with self.assertRaises(APIException) as e:
            omdb_movie(self.invalid_movie_title)
        self.assertEqual(e.exception.get_codes(), status.HTTP_503_SERVICE_UNAVAILABLE)

