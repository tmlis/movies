from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status
from ..models import Movie


class MovieTest(APITestCase):
    def setUp(self):
        self.url = reverse('movie-list')

    def test_create_movie(self):
        data = {'title': 'Green Book'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_title(self):
        data = {'title': 'invalid_movie_title'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)

    def test_create_movie_already_existing(self):
        data = {'title': 'Green Book'}
        self.client.post(self.url, data, format='json')
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)

    def test_get_movies(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CommentTest(APITestCase):
    def setUp(self):
        self.url = reverse('comment-list')
        Movie.objects.create(title='title',omdb_data={'Title': 'title'})

    def test_create_comment(self):
        data = {'movie_id': 1, 'text': 'Green Book'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_movie_id(self):
        data = {'movie_id': 100, 'text': 'Green Book'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_comments(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
