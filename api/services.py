import requests
from movies.settings import OMDB_API_URL, OMDB_API_KEY
from rest_framework import status
from rest_framework.exceptions import APIException


def omdb_movie(title):
    """service function for retrieving movie from omdbapi based on title"""
    params = {'apikey': OMDB_API_KEY,
              't': title}
    response = requests.get(OMDB_API_URL, params=params)
    response.raise_for_status()
    if response.status_code is status.HTTP_200_OK:
        content = response.json()
        if content['Response'] == 'False':
            raise APIException(f"A movie called '{title}' were not found.", code=status.HTTP_503_SERVICE_UNAVAILABLE)
        return content
    raise APIException

