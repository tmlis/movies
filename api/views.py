from api.models import Movie, Comment
from api.serializers import MovieSerializer, CommentSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from api.services import omdb_movie
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework import mixins


class MovieViewSet(mixins.ListModelMixin,
                   mixins.RetrieveModelMixin,
                   viewsets.GenericViewSet):
    """
    Movie viewset providing endpoints:
     - POST /movies
     - GET /movies
    """
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

    def create(self, request, *args, **kwargs):
        # handle omdb_movie()
        try:
            omdb_data = omdb_movie(request.data['title'])
        except APIException as e:
            return Response(status=e.get_codes())
        title = omdb_data['Title']
        # check if movie already in application database
        if Movie.objects.filter(title=title):
            return Response('Movie already in database', status=status.HTTP_409_CONFLICT)

        movie = Movie(title=title, omdb_data=omdb_data)
        movie.save()
        return Response(MovieSerializer(movie).data, status=status.HTTP_201_CREATED)


class CommentViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    """
    Comment viewset providing endpoints:
     - POST /comments
     - GET /comments
    """
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def create(self, request, *args, **kwargs):
        try:
            movie_id = request.data['movie_id']
        except KeyError:
            return Response('movie_id must be integer value', status=status.HTTP_400_BAD_REQUEST)
        text = request.data['text']
        if text == '':
            return Response('Text field cannot be empty', status=status.HTTP_400_BAD_REQUEST)
        if Movie.objects.filter(id=movie_id):
            comment = Comment(movie_id=movie_id, text=text)
            comment.save()
            return Response(CommentSerializer(comment).data, status=status.HTTP_201_CREATED)
        else:
            return Response('Invalid movie id', status=status.HTTP_400_BAD_REQUEST)

