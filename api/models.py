from django.db import models
from jsonfield import JSONField
from django.db.models.functions import Upper


class Movie(models.Model):
    title = models.TextField(blank=False)
    omdb_data = JSONField()

    class Meta:
        ordering = (Upper('title'),)


class Comment(models.Model):
    movie_id = models.IntegerField(blank=False)
    text = models.TextField(blank=False)

    class Meta:
        ordering = ('movie_id',)
