# Movies REST API
Movie database REST API, interacting with omdbapi.

## Getting started
### Installing
pip install -r requirements.txt
### Testing
python manage.py test
## Built With
#####Third-party libraries:
######Django REST Framework
#####Database:
######SQLite
Selected due to the fact that it is default in Django, do not require user management, and is sufficient for simple API.


